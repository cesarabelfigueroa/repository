let minimunCoin = (array) => {
  let sortedArray = array.sort((a, b) => a - b);
  let sum = 0;

  for (let i = 0; i <= sortedArray.length - 1; i++) {
    let actualCoin = sortedArray[i];
    if (actualCoin > sum + 1) {
      return sum + 1;
    } else {
      sum += actualCoin;
    }
  }

  return sum + 1;
};

let square = (array) => {
  let square = array.map((value) => value ** 2);
  return square.sort((a, b) => a - b);
};

console.log(minimunCoin([1, 1, 1, 1, 1]));
